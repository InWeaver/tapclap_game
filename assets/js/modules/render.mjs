/**
 * Visualisation stuff
 */


import $ from 'jquery';
import debounce from 'debounce';
import {CELL_KINDS} from '../modules/game.mjs';

class Pawn {
    cell;
    sprite;
    coordinates = {
        top: 0,
        left: 0
    }

    constructor(cell) {
        this.cell = cell;
        switch (cell.getKind()) {
            case CELL_KINDS.tile:
                this.sprite = $('<div class="game_cell">');
                this.sprite.addClass('game_cell--' + cell.getFamilyColor());
                break;
            case CELL_KINDS.bonus:
                this.sprite = $('<div class="game_cell_bonus">');
                break;
        }

    }

    destroy() {
        this.sprite.remove();
    }
}

class Renderer {
    session;
    $root;
    level;
    pawns = {};
    aspect = 1.12;
    horizontal_unit;

    createPawn(cell) {
        let pawn = new Pawn(cell);
        let self = this;
        if (typeof (this.pawns[cell.id]) !== 'undefined') {
            throw 'pawn with this id already exists!';
        } else {
            this.pawns[cell.id] = pawn;
            this.$root.append(pawn.sprite);
            this.updatePawnVisuals(pawn);
            pawn.sprite.on('click', function () {
                if (self.session.accepts_input === true) {
                    let result = self.session.passActivateCell(pawn.cell);
                    if (result.rebuild_canvas === true) {
                        self.createWindow();
                    } else {
                        self.updatePawns(result.cells_to_remove);
                        $.each(result.new_cells, (i, new_cell) => {
                            self.createPawn(new_cell)
                        });
                    }

                }
            });
        }
    }

    updateCanvasDimensions() {
        let width = window.__game_renderer.$root.width();
        window.__game_renderer.horizontal_unit = width / window.__game_renderer.level.boundary_width;
        window.__game_renderer.$root.height(window.__game_renderer.aspect * width * window.__game_renderer.level.boundary_height / window.__game_renderer.level.boundary_width);
        $.each(window.__game_renderer.pawns, (i, pawn) => {
            window.__game_renderer.updatePawnVisuals(pawn)
        });
    }

    updatePawnVisuals(pawn) {
        let translate
        if (pawn !== false) {
            translate = 'translate(' + pawn.cell.getWidth() * this.horizontal_unit + 'px, ' + this.aspect * pawn.cell.getHeight() * this.horizontal_unit + 'px)';
            pawn.sprite.css('transform', translate);
            pawn.sprite.css('width', this.horizontal_unit + 'px');
            pawn.sprite.css('height', this.aspect * this.horizontal_unit + 'px');
        }
    }

    updatePawns(cells_to_remove) {
        $.each(cells_to_remove, (i, cell) => {
            this.removePawn(cell.id);
        });
        $.each(this.pawns, (i, pawn) => {
            if (pawn !== false) {
                this.updatePawnVisuals(pawn);
            }
        });
    }

    removePawn(id) {
        // TODO: animation;
        this.pawns[id].destroy();
        this.pawns[id] = false;
    }

    constructor($canvas, session) {
        this.$root = $canvas;
        this.session = session;
        this.level = this.session.getCurrentLevel();
        window.__game_renderer = this;
        window.onresize = debounce(window.__game_renderer.updateCanvasDimensions, 200);
    }

    createWindow() {
        this.$root.empty();
        this.pawns = {};
        this.level = this.session.getCurrentLevel();
        let rows = this.level.getAllCells();

        $.each(rows, (height, cols) => {
            $.each(cols, (width, cell) => {
                this.createPawn(cell);
            });
        });
        this.updateCanvasDimensions();
    }
}

export default Renderer;
