# Тестовое задание
Реализовать прототип игры в жанре “головоломка с механикой Blast”.

## Описание общей механики игры:

- Игра состоит из игрового поля произвольного размера N*M.
- В каждой ячейке поля находится игровой объект (далее именуемый тайл) определенного цвета.
- Количество возможных вариантов цветов равно C.
- Начальное состояние поля задается случайно (вероятность цвета тайла является равновероятной).

При клике на тайл сжигается (удаляется) область (фигура), состоящая из группы прилегающих тайлов того же цвета, размер группы не может быть меньше чем K (по умолчанию K=2).

После сжигания тайлов, образуются пустые клетки. Далее происходит перемещение соседних тайлов в пустые клетки согласно заданной физике перемещения тайлов на поле.

Если соседние тайлы, доступные для перемещения, отсутствуют, то пустые ячейки заполняются случайно сгенерированными тайлами (вероятность цвета тайла является равновероятной).

Процесс перемещения и добавление новых тайлов происходит до тех пор, пока поле снова не будет полностью заполнено.
На заполненном поле всегда можно сжечь тайлы:
- если такой возможности нет, то необходимо перемешать тайлы на поле (количество перемешиваний S)
- если же после перемешивания нет возможности сжечь тайлы, то такая ситуация является проигрышем для игрока.

Цель игры – набрать X очков за Y ходов, иначе проигрыш.
Значение количества очков и ходов для выигрыша, а также формула начисления очков остается на усмотрение соискателя.

## Основное задание:

Реализовать общую механику игры на JavaScript + HTML5
Реализовать анимации для перемещения и сжигания тайлов
Отображение количества оставшихся ходов и набранных очков
Обработать состояние выигрыша или проигрыша
Использовать приложенный набор ассетов
Исходники выложить на github.com / gitlab.com / bitbucket.com

## Будет большим плюсом:

- Использовать при разработке принципы SOLID
- Отделить логику игры и отображение
- Разбить игру на отдельные состояния (сцены)
- Применение модульного тестирования
- Атомарные коммиты в репозитории
- Использование ES6 + Babel
- Сборка проекта gulp / webpack

## Дополнительные задания на выбор (необязательные):

### Реализовать “бустер бомба”
При активации бустера и клика по полю, в данной клетке сжигаются тайлы в радиусе R клеток.

### Реализовать “бустер перемешать поле”
При активации бустера все тайлы на поле перемешиваются.

### Реализовать механику “супер тайла”

Если при уничтожении размер группы тайлов больше чем L, то тогда на месте клетки, по которой был клик, появится новый тайл.
По клику на него активизируется определенная логика, возможные варианты:
- Сжигается вся строка в которой находится тайл
- Сжигается весь столбец в котором находится тайл
- Сжигаются тайлы в радиусе R клеток
- Сжигается всё поле


### Список игр с механикой “Blast”:
https://play.google.com/store/apps/details?id=com.rovio.blast
https://play.google.com/store/apps/details?id=com.superbox.aos.jewelblast
https://play.google.com/store/apps/details?id=toy.blast.pop.cubes.puzzle


### notes
`nodejs - v14.17.0`

OS WINDOWS
```
Major  Minor  Build  Revision
-----  -----  -----  --------
10     0      17763  0
```

