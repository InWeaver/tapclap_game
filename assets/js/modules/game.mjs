/**
 * Game business logic goes here...
 *
 * coordinates are defined by width and height
 *
 * width goes left->right
 * height goes top->down
 *
 * example:
 * [00][10][20]
 * [01][11][21]
 * [02][12][22]
 */

import Rand from 'rand-seed'; // gotta make random repeatable;
import FloodFillSearch from '../modules/floodfill.mjs';


const DEFAULT_SEED = '1234'; // what cells to place
const CELL_KIND_TILE = 0; // just tile
const CELL_KIND_BONUS = 1; // bombs, scrambles, other stuff

// TODO move to class???
const BONUS_BOMB = 0;
const BOMB_RADIUS = 2;


// where to move tile after deletion
const PHYSICS_RULE_RIGHT = 0;
const PHYSICS_RULE_DOWN = 1;
const PHYSICS_RULE_LEFT = 2;
const PHYSICS_RULE_UP = 3;

/**
 * helps differentiate cells
 */
class CellFamily {
    // bro, there's probably a shorter way to code this
    index;
    color;

    constructor(index, color) {
        this.index = index;
        this.color = color;
    }
}

/**
 * "PURE" stuff I dunno where to put
 */
class Utils {
    // shoulda make families classy too, doge
    static #families = {
        0: 'blue',
        1: 'green',
        2: 'red',
        3: 'violet',
        4: 'yellow',
    };

    static getRandomFamily(random_value) {
        let max = Object.keys(this.#families).length;
        let family_index = Math.floor(random_value * max);
        return new CellFamily(family_index, this.#families[family_index]);
    }

    static getRandomKind(random_value) {
        let kind = random_value > 0.05 ? CELL_KIND_TILE : CELL_KIND_BONUS;
        return kind;
    }
}

/**
 * Single cell data
 */
class Cell {
    // TODO create class for coordinates
    #coords = {
        width: 0,
        height: 0
    }
    id;
    kind; // defines behavior
    #family; // defines color
    bonus_type; // bonus behavior

    getCoordinates() {
        return this.#coords;
    }

    getWidth() {
        return this.#coords.width;
    }

    getHeight() {
        return this.#coords.height;
    }

    setCoordinates(width, height) {
        this.#coords = {
            width: width,
            height: height
        }
    }

    getKind() {
        return this.kind;
    }

    getFamilyIndex() {
        return this.#family.index;
    }

    getFamilyColor() {
        return this.#family.color;
    }

    constructor(position_w, position_h, family, kind, id, bonus_type = BONUS_BOMB) {
        this.id = id;
        this.#coords.width = position_w;
        this.#coords.height = position_h;
        this.#family = family;
        this.kind = kind;
        this.bonus_type = bonus_type;
    }
}

class Level {
    boundary_width;
    boundary_height;
    #cells = [];
    #prng;
    #physics_rule;
    #min_cells_to_remove;
    #id_counter = 0;


    constructor(width, height, seed, min_cells_to_remove, physics_rule) {
        this.#prng = new Rand(seed);

        this.boundary_width = width;
        this.boundary_height = height;
        this.#min_cells_to_remove = min_cells_to_remove;
        this.#physics_rule = physics_rule;

        let row;
        // fill level with tiles
        for (let h = 0; h < this.boundary_height; h++) {
            row = []
            for (let w = 0; w < this.boundary_width; w++) {
                row.push(
                    new Cell(
                        w,
                        h,
                        Utils.getRandomFamily(this.#prng.next()),
                        CELL_KIND_TILE,
                        this.createId()
                    )
                )
            }
            this.#cells[h] = row;
        }
    }

    createId() {
        this.#id_counter++;
        return this.#id_counter;
    }

    getAllCells() {
        return this.#cells;
    }

    cellIsFree(width, height) {
        let inbounds = this.pointIsInBounds(width, height);
        if (inbounds === true) {
            if (this.#cells[height][width] === false) {
                return true
            }
        }
        return false;
    };

    pointIsInBounds(width, height) {
        if (0 <= width && width < this.boundary_width) {
            if (0 <= height && height < this.boundary_height) {
                return true;
            }
        }
        return false;
    }

    getCell(width, height) {
        if (this.pointIsInBounds(width, height) === true) {
            return this.#cells[height][width];
        }
        return false;
    }

    setCell(cell, width, height) {
        this.#cells[height][width] = cell;
    }

    performCellPhysics(cell, offset_width, offset_height) {
        // greeedy recursive offset method
        if (cell !== false) {
            let new_width = cell.getWidth() + offset_width;
            let new_height = cell.getHeight() + offset_height;
            let free = this.cellIsFree(new_width, new_height);
            if (free === true) {
                this.setCell(false, cell.getWidth(), cell.getHeight());
                cell.setCoordinates(new_width, new_height);
                this.setCell(cell, new_width, new_height);
                this.performCellPhysics(cell, offset_width, offset_height);
            }
        }
    }

    removeCell(cell) {
        this.#cells[cell.getHeight()][cell.getWidth()] = false;
    }

    doPhysics() {
        console.log(this.#physics_rule)
        switch (this.#physics_rule) {
            case PHYSICS_RULE_RIGHT:
                // iterate over each row right to left;
                for (let h = 0; h < this.boundary_height; h++) {
                    for (let w = this.boundary_width - 1; w > -1; w--) {
                        this.performCellPhysics(this.getCell(w, h), 1, 0);
                    }
                }
                break;
            case PHYSICS_RULE_LEFT:
                // iterate over each row right to left;
                for (let h = 0; h < this.boundary_height; h++) {
                    for (let w = 0; w < this.boundary_width; w++) {
                        this.performCellPhysics(this.getCell(w, h), -1, 0);
                    }
                }
                break
            case PHYSICS_RULE_DOWN:
                // iterate over each col from bottom to top
                for (let w = 0; w < this.boundary_width; w++) {
                    for (let h = this.boundary_height - 1; h > -1; h--) {
                        this.performCellPhysics(this.getCell(w, h), 0, 1);
                    }
                }
                break;
            case PHYSICS_RULE_UP:
                // iterate over each col from top to bottom
                for (let w = 0; w < this.boundary_width; w++) {
                    for (let h = 0; h < this.boundary_height; h++) {
                        this.performCellPhysics(this.getCell(w, h), 0, -1);
                    }
                }
                break;
        }
    }

    fillEmptyCells() {
        let cell, new_cells = [];
        for (let h = 0; h < this.boundary_height; h++) {
            for (let w = 0; w < this.boundary_width; w++) {
                if (this.cellIsFree(w,h) === true) {
                    cell = new Cell(
                        w,
                        h,
                        Utils.getRandomFamily(this.#prng.next()),
                        Utils.getRandomKind(this.#prng.next()),
                        this.createId(),
                    )
                    this.setCell(cell,w,h);
                    new_cells.push(cell);
                }
            }
        }

        return new_cells;
    }

    searchBombCells(cell) { //TODO add chain reaction???
        let cells_to_remove = [];
        let removed_cell;
        let start_width = cell.getWidth() - BOMB_RADIUS;
        let end_width = cell.getWidth() + BOMB_RADIUS;
        let start_height = cell.getHeight() - BOMB_RADIUS;
        let end_height = cell.getHeight() + BOMB_RADIUS;

        for (let w = start_width; w <= end_width; w++) {
            for (let h = start_height; h <= end_height; h++) {
                removed_cell = this.getCell(w,h);
                if (removed_cell !== false) {
                    cells_to_remove.push(removed_cell);
                }
            }
        }
        return cells_to_remove;
    }

    activateCell(cell) {
        let cells_to_remove = [];
        let result = {
            cells_to_remove: [],
            new_cells: []
        }

        switch (cell.kind) {
            case CELL_KIND_TILE:
                cells_to_remove = FloodFillSearch.findNeighbours(this, cell);
                break;
            case CELL_KIND_BONUS:
                cells_to_remove = this.searchBombCells(cell);
                break;
        }

        if (cells_to_remove.length >= this.#min_cells_to_remove) {
            for (const found_cell of cells_to_remove) {
                this.removeCell(found_cell)
            }
            this.doPhysics();
            result.new_cells = this.fillEmptyCells();
        } else {
            cells_to_remove = [];
        }

        result.cells_to_remove = cells_to_remove;
        return result;
    }
}

/**
 * GOD-class, sorry
 * Current session data and business logic interface
 */
class GameSession {
    #defaults = {
        min_width: 2,
        min_height: 2,
        max_width: 100,
        max_height: 100,
        score_per_cell: 50,
        min_cells_to_remove: 3,
        physics_rule: PHYSICS_RULE_RIGHT
    };
    #current_level;
    current_level_counter = 0;
    last_level_index;
    accepts_input = true;
    levels;
    ui;
    player;
    seed;
    state;
    required_score;
    moves;

    constructor(seed = DEFAULT_SEED, ui, player, levels) {
        this.seed = seed;
        this.ui = ui;
        this.player = player;
        this.levels = levels;
        this.last_level_index = levels.length-1;
        this.createLevel(levels[this.current_level_counter]);
    }

    updateUi () {
        this.ui.setLevel(this.current_level_counter + 1);
        this.ui.setMoves(this.moves);
        this.ui.setMovesLeft(this.player.moves_left);
        this.ui.setScore(this.player.score);
        this.ui.setRequired(this.required_score);
    }

    createLevel(level) {
        // check input, duh.
        let width = level.width;
        let height = level.height;
        let level_okay = false;
        if (this.#defaults.min_width <= width && width <= this.#defaults.max_width) {
            if (this.#defaults.min_height <= height && height <= this.#defaults.max_height) {
                level_okay = true;
            }
        }
        if (!level_okay) {
            throw 'Wrong level dimensions, bro!';
        } else {
            this.required_score = level.required_score;
            this.moves = level.moves;
            this.player.moves_left = level.moves;
            this.#current_level = new Level(width, height, this.seed, this.#defaults.min_cells_to_remove, level.physics);
            this.updateUi();
        }
    }

    nextLevel() {
        this.player.score = 0;
        this.current_level_counter++;
        alert('Good job!');
        if (this.current_level_counter > this.last_level_index) {
            alert('Grats, ye finished all the levels!');
            this.current_level_counter = 0;
        }
        this.createLevel(this.levels[this.current_level_counter]);
    }

    gameOver() {
        alert('Try again');
        this.player.score = 0;
        this.createLevel(this.levels[this.current_level_counter]);
    }

    passActivateCell(cell) {
        let result, removed_cells_count, rebuild_canvas;
        this.accepts_input = false;
        rebuild_canvas = false;
        result = this.#current_level.activateCell(cell);
        removed_cells_count = result.cells_to_remove.length;
        if (removed_cells_count > 0) {
            this.player.moves_left--;
            if (this.player.moves_left <=0) {
                this.gameOver();
                rebuild_canvas = true;
            } else {
                this.player.score += removed_cells_count * this.#defaults.score_per_cell;
                if (this.player.score >= this.required_score) {
                    this.nextLevel();
                    rebuild_canvas = true;
                }
            }
            this.updateUi();
        }
        this.accepts_input = true;
        result.rebuild_canvas = rebuild_canvas;
        return result;
    }

    getCurrentLevel() {
        return this.#current_level;
    }
}

export default GameSession;
export const PHYSICS_RULES = {
    'right': PHYSICS_RULE_RIGHT,
    'down': PHYSICS_RULE_DOWN,
    'left': PHYSICS_RULE_LEFT,
    'up': PHYSICS_RULE_UP
};
export const CELL_KINDS = {
    'tile': CELL_KIND_TILE,
    'bonus': CELL_KIND_BONUS
}

export const BONUS_TYPES = {
    'bomb': BONUS_BOMB,
}
