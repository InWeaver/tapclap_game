let gulp = require('gulp');
let rename = require('gulp-rename');

let browserSync = require('browser-sync');
let fileinclude = require('gulp-file-include');

let browserify = require('browserify')
let babelify = require('babelify')

let source = require('vinyl-source-stream');
let buffer = require('vinyl-buffer')

let sourcemaps = require('gulp-sourcemaps');
let uglify = require('gulp-uglify')

let imagemin = require('gulp-imagemin');
let clean = require('gulp-clean');

const TEMPLATES_DIR = './assets/templates/'
const JS_DIR = './assets/js/'
const IMG_DIR = './assets/img/'
const INDEX_JS = './assets/js/app.js'
const DIST_DIR = './dist/';


gulp.task('browser-sync', function (callback) {
    browserSync({
        port: 9000,
        open: false,
        server: {
            baseDir: DIST_DIR,
            index: 'index.html'
        },
        notify: true,

    });
    callback();
});

gulp.task('compile:html', function () {
    return gulp.src([TEMPLATES_DIR + 'index.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest(DIST_DIR))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('compile:js', function () {
    return browserify({entries: [INDEX_JS]})
        .transform(babelify, {presets: ["@babel/preset-env"]})
        .bundle()
        .pipe(source(INDEX_JS))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        // .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(rename({dirname: 'js'}))
        .pipe(gulp.dest(DIST_DIR))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('compile:img', function() {
  return gulp.src([IMG_DIR+'**/*.*'])
    // .pipe( imagemin() )
    .pipe( gulp.dest(DIST_DIR+'img/') )
    .pipe( browserSync.reload({stream: true}) )
});

gulp.task('watch', function (callback) {
    gulp.watch(TEMPLATES_DIR + '**/*.html', gulp.series(['compile:html']));
    gulp.watch(JS_DIR + '**/*.*js', gulp.series(['compile:js']));
    callback();
})

gulp.task('compile',
    gulp.parallel(['compile:html']),
    gulp.parallel(['compile:js']),
    gulp.parallel(['compile:img']),
);

gulp.task('clean', function() {
    return gulp.src(DIST_DIR, {read: false})
        .pipe(clean())
})

gulp.task('default', gulp.parallel([
        'browser-sync',
        'watch',
        'compile'
    ]
));
