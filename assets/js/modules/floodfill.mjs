/**
 * Finds adjacent cells of a same colour;
 * loosely based on FloodFill algorithm
 * https://en.wikipedia.org/wiki/Flood_fill
*/

class FloodFillSearch {
    static #hash_it(width, height) {
        return width.toString() + height.toString();
    }

    static #fillSearch(grid, index, width, height, visited, found) {
        let new_index, cell;
        let hash = this.#hash_it(width, height);

        if (visited.indexOf(hash) < 0) {
            visited.push(hash);
            cell = grid.getCell(width, height);
            if (cell !== false) {
                new_index = cell.getFamilyIndex();
                if (new_index === index) {
                    found.push(cell);
                    this.#fillSearch(grid, index, width + 1, height, visited, found);
                    this.#fillSearch(grid, index, width - 1, height, visited, found);
                    this.#fillSearch(grid, index, width, height + 1, visited, found);
                    this.#fillSearch(grid, index, width, height - 1, visited, found);
                }
            }
        }
    }

    static findNeighbours(grid, cell) {
        let index, visited;
        let found = [];
        let width = cell.getWidth();
        let height = cell.getHeight();
        index = cell.getFamilyIndex();
        visited = [];
        this.#fillSearch(grid, index, width, height, visited, found);
        return found;
    }
}

export default FloodFillSearch;
