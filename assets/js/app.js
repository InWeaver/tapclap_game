"use strict";

import $ from 'jquery';
import GameSession, {PHYSICS_RULES} from './modules/game.mjs';
import Renderer from './modules/render.mjs';
import Player from './modules/flow.mjs';
import {LevelSettings} from './modules/flow.mjs';


let SEED = '1234';
let MIN_CELLS_TO_SCORE = 2;

let levels = [
    new LevelSettings(
        1,
        8,
        5,
        1000,
        5,
        PHYSICS_RULES.down
    ),
    new LevelSettings(
        2,
        6,
        8,
        1500,
        7,
        PHYSICS_RULES.up
    ),
    new LevelSettings(
        3,
        8,
        12,
        3000,
        10,
        PHYSICS_RULES.left
    ),
    new LevelSettings(
        4,
        10,
        14,
        6000,
        10,
        PHYSICS_RULES.right
    ),
]

let bindUi = function() {
    let $canvas = $('#canvas');
    let $score = $('#js-score');
    let $score_required = $('#js-score-required');
    let $moves = $('#js-moves');
    let $moves_left = $('#js-moves-left');
    let $level = $('#js-level');
    let ui = {
        $canvas: $canvas,
        setLevel: function(val) {
            $level.html(val);
        },
        setScore: function(val) {
            $score.html(val);
        },
        setRequired: function(val) {
            $score_required.html(val);
        },
        setMoves: function(val) {
            $moves.html(val);
        },
        setMovesLeft: function(val) {
            $moves_left.html(val);
        }
    }
    return ui
}

let init = function (){
    let player = new Player();
    let ui = bindUi();
    let session, renderer, level;

    session = new GameSession(SEED, ui, player, levels);
    renderer = new Renderer(ui.$canvas, session);
    renderer.createWindow();
}

init();




