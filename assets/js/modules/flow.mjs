export default class Player {
    score;
    moves_left;
    constructor(score=0, moves_left=0) {
        this.score = score;
        this.moves_left = moves_left;
    }
}

export class LevelSettings {
    level_number;
    width;
    height;
    required_score;
    moves;
    physics;

    constructor(level_number=1, width, height, required_score, moves, physics) {
        this.width = width;
        this.height = height;
        this.level_number = level_number;
        this.required_score = required_score;
        this.moves = moves;
        this.physics = physics
    }
}

